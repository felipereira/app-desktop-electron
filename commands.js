var http = require('http');
const util = require('util');
var fs = require('fs');
var xml2js = require('xml2js');

const log = require('simple-node-logger').createSimpleFileLogger('log/comandos.log');

const centralDistinta =
    {
        port: 80,
        path: '/iasws115/iasws.asmx',
        method: 'POST',
        headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
        }
    };

const rastreador =
    {
        id: '',
        hosts:
        [
            // "10.40.1.75",
            "10.40.3.246"
        ],

        user: "teste",
        password: "zatix",
    };


var configXML;
var SOAPAction = 'http://microsoft.com/webservices/'
var count = 0;
const commands = ["Desativar", "Ativar Modo Interativo", "Bloquear Rastreador", "Desbloquear Rastreador", "Ativar Modo Rastreado", "Deslacrar Bau", "Lacrar Bau"];
const resultWS = ["Desativar", "AtivarModoInterativo", "BloquearRastreador", "DesbloquearRastreador", "AtivarModoRastreado", "DeslacrarBau", "LacrarBau"];

function sendCommand() {

    var num = document.getElementById('ri').value;
    ns = num.toString().length;

    num1 = num.toUpperCase();
    var validate_ns = num1.replace(/[^A-Z]/g, '');
    validate_ns = validate_ns.toString().length;

    if (ns != 6 && ns > 0 || validate_ns > 0) {
        
        alert("Numero de Série Inválido");
    }

    if (ns == 6 && validate_ns == 0) {

        rastreador.id = num;

        for (let _i = 0; _i < commands.length; _i++) {

            if (document.getElementById('tags').value == commands[_i] && count > 0) {

                count = 0;
                console.log("Comando selecionado: ", commands[_i]);

                fs.readFile('arquivos/' + commands[_i] + '.xml', function (err, data) {

                    var parser = new xml2js.Parser();
                    parser.parseString(data, function (err, result) {

                        var builder = new xml2js.Builder();
                        configXML = builder.buildObject(result);

                        configXML = configXML.replace("user", rastreador.user).
                            replace("password", rastreador.password).
                            replace("idvehicle", rastreador.id);

                        for (_j = 0; _j < rastreador.hosts.length; _j++) {

                            centralDistinta.hostname = rastreador.hosts[_j];
                            var req = http.request(centralDistinta, (res) => {
                                res.setEncoding('utf8');
                                let data = '';
                                res.on('data', d => data += d);
                                res.on('end', () => {
                                    parser.parseString(data, function (err, result) {

                                        var builder = new xml2js.Builder();

                                        if (typeof Object.keys(result) !== undefined && Object.keys(result).length > 0) {
                                            var responseWS = builder.buildObject(result);

                                            responseWS = responseWS.replace(/<.*soap.*>/g, "")
                                                .replace(new RegExp("\\&" + "lt;", 'g'), "<")
                                                .replace(new RegExp("\\&" + "gt;", 'g'), ">")
                                                .replace("\n", "");

                                            var parser2 = new DOMParser();
                                            var xmlDoc = parser2.parseFromString(responseWS, "text/xml");

                                            var idevent = "0";

                                            if (xmlDoc.getElementsByTagName(resultWS[_i] + 'Result')[0] != undefined) {

                                                idevent = xmlDoc.getElementsByTagName(resultWS[_i] + 'Result')[0].childNodes[0].nodeValue;
                                                var idseq = parseInt(idevent);

                                                if (idseq > 0 && idseq != undefined) {
                                                    alert(new Date().toJSON() + "- Comando enviado com sucesso para o rastreador ", rastreador.id);
                                                    console.log(new Date().toJSON() + "- Comando enviado com sucesso para o rastreador: ", rastreador.id);
                                                }

                                            } else {
                                                console.log(new Date().toJSON() + " Erro na requisicao", + idseq)
                                                log.info(new Date().toJSON() + " Erro na requisicao", + idseq)
                                            }

                                        } else if (typeof Object.keys(result) === undefined || Object.keys(result).length < 0) {

                                            console.log(new Date().toJSON() + " Timeout requisicao");
                                            log.info(new Date().toJSON() + " Timeout requisicao")
                                        }
                                    });
                                });
                            });

                            req.on('error', (e) => {
                                console.log(new Date().toJSON() + ` Problema na conexao: ${e.message}`);
                                log.info(new Date().toJSON() + ` Problema na conexao: ${e.message}`);
                            });

                            req.write(configXML);
                            req.end();
                        }
                    });
                });
            }
        }
    } else if ( ns == 0) {
        alert("Digite o Número de Série");
    }
}


