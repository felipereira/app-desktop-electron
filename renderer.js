var http = require('http');
const util = require('util');
var fs = require('fs');
var xml2js = require('xml2js');
var DOMParser = require('xmldom').DOMParser;

const log = require('simple-node-logger').createSimpleFileLogger('log/consumidor.log');

const SOAPAction = 'http://microsoft.com/webservices/';
const centralDistinta =
    {
        port: 80,
        path: '/iasws/iasws.asmx',
        method: 'POST',
        headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
            'SOAPAction': ''
        }
    };

const centrais =
    {
        hosts: [
            "10.40.3.246",
            "10.40.1.75"
        ]
    };
var last_idseq = -1;
const methodsWS = ["ObtemEventosNormais", "ObtemEventosCtrl"];

var flag = false;

function setFlag(bool) {

    flag = bool;

    if (flag == true) {
        alert("Consumo de Teleeventos Executado.");
    } else if (flag == false) {
        alert("Consumo de Teleeventos Pausado.")
    }
}

var timer =
    setInterval(function () {
        if (flag)
            ProcessaEventos();
    }, 1000);

function ProcessaEventos() {

    var configXML;
    for (let _i = 0; _i < methodsWS.length; _i++) {

        fs.readFile('file/' + methodsWS[_i] + '.xml', function (err, data) {

            var parser = new xml2js.Parser();
            parser.parseString(data, function (err, result) {

                var builder = new xml2js.Builder();
                configXML = builder.buildObject(result);

                for (_j = 0; _j < centrais.hosts.length; _j++) {

                    centralDistinta.hostname = centrais.hosts[_j];
                    centralDistinta.headers.SOAPAction = SOAPAction + methodsWS[_i];

                    var req = http.request(centralDistinta, (res) => {
                        res.setEncoding('utf8');
                        let data = '';
                        res.on('data', d => data += d);
                        res.on('end', () => {
                            parser.parseString(data, function (err, result) {

                                var builder = new xml2js.Builder();

                                try {

                                    if (typeof Object.keys(result) !== undefined && Object.keys(result).length > 0) {

                                        var responseWS = builder.buildObject(result);

                                        responseWS = responseWS.replace(/<.*soap.*>/g, "")
                                            .replace(new RegExp("\\&" + "lt;", 'g'), "<")
                                            .replace(new RegExp("\\&" + "gt;", 'g'), ">")
                                            .replace("\n", "")
                                            .replace(/<.*ObtemEventosRespons.*>/g, "");

                                        var parser2 = new DOMParser();
                                        var xmlDoc = parser2.parseFromString(responseWS, "text/xml");

                                        var idevent = "0";

                                        if (methodsWS[_i] == "ObtemEventosCtrl") {

                                            if (xmlDoc.getElementsByTagName("NumeroSequenciaCtrl")[0] != undefined) {

                                                idevent = xmlDoc.getElementsByTagName("NumeroSequenciaCtrl")[0].childNodes[0].nodeValue;
                                                var idseq = parseInt(idevent);
                                                configXML = configXML.replace("<UltimoSequencialCtrl>9999999</UltimoSequencialCtrl>", '<UltimoSequencialCtrl>' + idseq + '</UltimoSequencialCtrl>');

                                                if (idseq != last_idseq) {

                                                    last_idseq = idseq;

                                                    log.info(new Date().toJSON() + ' Cosumindo Teleevento do metodo ' + methodsWS[_i] + ": ", (idseq++) + ' ');
                                                    console.log(new Date().toJSON() + ": " + methodsWS[_i] + " Cosumindo Teleevento: ", idseq++);
                                                }
                                            }
                                        }

                                        else if (methodsWS[_i] == "ObtemEventosNormais") {

                                            if (xmlDoc.getElementsByTagName("NumeroSequencia")[0] != undefined) {

                                                idevent = xmlDoc.getElementsByTagName("NumeroSequencia")[0].childNodes[0].nodeValue;
                                                var idseq = parseInt(idevent);
                                                configXML = configXML.replace("<UltimoSequencial>9999999</UltimoSequencial>", '<UltimoSequencial>' + idseq + '</UltimoSequencial>');

                                                if (idseq != last_idseq) {

                                                    last_idseq = idseq;

                                                    log.info(new Date().toJSON() + ' Cosumindo Teleevento do metodo ' + methodsWS[_i] + ": ", (idseq++) + ' ');
                                                    console.log(new Date().toJSON() + ": " + methodsWS[_i] + " Cosumindo Teleevento: ", idseq++);
                                                }
                                            }
                                        }

                                        else if (typeof Object.keys(result) === undefined || Object.keys(result).length < 0) {

                                            setInterval(function () {
                                                ProcessaEventos();
                                            }, 1000);
                                        }
                                    }
                                } catch (err) {
                                    console.log(new Date().toJSON() + "  Aguardando resposta");
                                    log.info(new Date().toJSON() + " Aguardando conexao")
                                }
                            });
                        });
                    });

                    req.on('error', (e) => {
                        console.log(new Date().toJSON() + ` Problea na conexao: ${e.message}`);  
                        log.info(new Date().toJSON() + ` Problema na conexao: ${e.message}`);
                    });

                    req.write(configXML);
                    req.end();
                }
            })
        })
    }
}



